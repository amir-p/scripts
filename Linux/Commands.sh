#!/bin/bash

########################################################
#cat

#cat - show file with line numbers
cat -n file.txt

#cat - quick create new file with STDIN provided content
cat > file.txt 

#cat - concatenate multiple files to one
cat file1.txt file2.txt file3.txt > result.txt

########################################################


########################################################
#head

#head - prints first 15 lines from file.txt
head -n 15 file.txt

ls /usr/bin | head -n 5


#tail

#tail - follow the file and if new content is addded it will print it
tail -f file.txt

ls /usr/bin | tail -n 5
########################################################


########################################################
#find

#find and cound all files(all tyoe if files) in home directory
find ~ | wc -l

#find all files(in current directory) which have extension of .pdf and is bigger then 10Mb
#and delete all these files matching this criteria
find ~/TestBash/TestPdfs -maxdepth 1 -type f -and -name '*.pdf' -and -size +10M –and -delete 

#find with custom action ls
find . -type f -and -name '*.pdf' -and -size +10M -exec ls -l '{}' ';' 

#find with custom action ls, propmpting before every ls command
find . -type f -and -name '*.pdf' -and -size +10M -ok ls -l '{}' ';' 

#passing results from find command parameters from find to custom action
find . -type f -and -name '*.pdf' -and -size +10M -exec ls -l '{}' '+'

find . -type f -and -name '*.pdf' -and -size +10M | xargs ls -l

#find with ignore case
find . -iname '*.php' 

#find with subdirectory depth(only current directory)
find . -maxdepth 1 -name testFile.txt 

#find with multiple directories
find ~/Documents ~/src -name testFile.txt 

#find hidden files
find ./TestBash -name ".*" 

#find empty files and directories
find ~ -type f -empty 
find ~ -type d -empty 


########################################################
#locate

#update database manually
sudo updatedb

#locate count
locate –c fileName

#force locate
locate -e "*fileToSearch.txt*" 

#locate ignore case
locate -i "*fileToSearch.txt*" 

#locate exact file name
locate –r /NAME1$ 

########################################################
#grep

grep 'Test123' testFile.txt

#grep ignore case
grep i 'Test123' testFile.txt

#grep recursive search
grep -R 'home'

#########################################################
#chmod

#add mode
chmod u+w,a+r filename.txt  

#change mode
chmod u=w,a=r filename.txt  

#########################################################
#General

#open Explorer app in current directory
xdg-open . 



