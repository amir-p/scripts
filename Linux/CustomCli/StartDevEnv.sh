#!/bin/bash

echo 'Starting Dev Env applications:'

code &
terminator &
keepassx &
brightness-controller &
subl &
google-chrome &
firefox &

#Open Current Notes, Sublime
#Open Current Projects, VS Code

echo 'Done!' &
exit 0
