#!/bin/bash

function cmd-help(){
	echo -e
	echo 'Commands: '
	echo -e
	echo -e
	echo 'Applications Start:'
	echo 'cmd-start-dev-env  - will start the applications needed for Development environment'
	echo 'cmd-start-common-env  - will start the applications needed for Common environment'
	echo -e
	echo -e
	echo 'Ubuntu Container:'
	echo 'cmd-init-ubuntu  - Creates Ubuntu docker container and runs bash shell.'
	echo 'cmd-remove-ubuntu  - Remove Ubuntu docker container.'
	echo 'cmd-start-ubuntu  - Start Ubuntu docker container and runs bash shell.'
	echo 'cmd-stop-ubuntu  - Stop Ubuntu docker container.'
	echo -e
}

function cmd-start-dev-env() {
	bash ~/src/git/scripts/Linux/CustomCli/StartDevEnv.sh
}

function cmd-start-common-env() {
	bash ~/src/git/scripts/Linux/CustomCli/StartCommonEnv.sh
}

function cmd-init-ubuntu() {
	docker run -it --name customcliubuntu ubuntu:latest /bin/bash
}

function cmd-remove-ubuntu() {
	docker stop customcliubuntu
	docker rm customcliubuntu	
}

function cmd-start-ubuntu() {
	docker start customcliubuntu
	docker exec -it customcliubuntu /bin/bash
}

function cmd-stop-ubuntu() {
	docker stop customcliubuntu
}