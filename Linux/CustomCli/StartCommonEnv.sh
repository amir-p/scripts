#!/bin/bash

echo 'Starting default applications:'

terminator &
keepassx &
brightness-controller &
google-chrome &
firefox &

echo 'Done!' &
exit 0
