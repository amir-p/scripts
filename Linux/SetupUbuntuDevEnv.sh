#!/bin/bash

cd ~ && mkdir SetupDevEnvScript && cd SetupDevEnvScript
mkdir Logs && touch Logs/SetupUbuntuDevEnvRunSummary.txt

echo 'Upgrading packages!'

#update and upgrade packages
apt-get update -y && apt-get upgrade -y

installedSoftware=()
versions=""
#failedInstall=()

#Install nano
apt-get install nano

#Install curl 
apt install curl -y;
packageNotFound="no packages found matching"

#Install wget
apt-get install wget -y
installedSoftware+=('wget')

#Install Snapd(Snap)
apt-get install snapd -y

#Install unrar
apt-get install unrar

#Install git 
apt install git -y
installedSoftware+=('git')
gitVersion="$(git --version)" && gitVersion="Git: $gitVersion"
versions="$versions \n$gitVersion"

#Install Docker 
apt install apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
apt update -y
#apt-cache policy docker-ce
apt install docker-ce -y
systemctl status docker

#apt-get install docker-compose -y 
#will install most probably not the up to date version of docker-compose
# if you want one of the latest you can use:

curl -L https://github.com/docker/compose/releases/download/1.24.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

installedSoftware+=('docker')
installedSoftware+=('docker-compose')
dockerVersion="$(docker -v)" && dockerVersion="Docker: $dockerVersion"
versions="$versions \n$dockerVersion"

# Create Docker user group and add user to it
groupadd docker
usermod -aG docker $USER

#Install .NET 5.0 and aspnetcore Runtime 5.0
sudo apt-get update; \
sudo apt-get install -y apt-transport-https && \
sudo apt-get update && \
sudo apt-get install -y dotnet-sdk-5.0

sudo apt-get update; \
sudo apt-get install -y apt-transport-https && \
sudo apt-get update && \
sudo apt-get install -y aspnetcore-runtime-5.0

installedSoftware+=('.NET 5')
dotnetVersion="$(dotnet --version)" && dotnetVersion=".NET: $dotnetVersion"
versions="$versions \n$dotnetVersion"

#Install NVM(Node version manager)
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash

#Install NodeJs and npm
# curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh
# bash nodesource_setup.sh
# apt-get install nodejs -y
# apt-get install npm -y
# npm install -g npm

# installedSoftware+=('nodejs')
# installedSoftware+=('npm')
# nodejsVersion="$(node -v)" && nodejsVersion="NodeJs: $nodejsVersion" 
# npmVersion="npm: $(npm -v)" && npmVersion="npm: $npmVersion"
# versions+=($nodejsVersion)
# versions+=($npmVersion)
# versions="$versions \n$nodejsVersion \n$npmVersion"

#Install Python 3
apt-get install python3.7 -y
installedSoftware+=('python 3')
pythonVersion="$(python3 -V)" && pythonVersion="Python: $pythonVersion" 
versions+=($pythonVersion)
versions="$versions \n$pythonVersion"

#Install VS Code
apt updatesudo apt install software-properties-common apt-transport-https wget
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | apt-key add -
add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
apt install code -y
vsCodeVersion="$(code -v)" && vsCodeVersion="VS Code: $vsCodeVersion"
installedSoftware+=('vs code')
versions="$versions \n$vsCodeVersion"

#Install Angular
npm install -g @angular/cli
installedSoftware+=('angular')
angularVersion="$(ng --version)" && angularVersion="AngularJS: $angularVersion"
versions="$versions \n $angularVersion"

#Install Keepassx  
apt-get install keepassx -y 
installedSoftware+=('keepassx')

#Install Brightness Controll 
add-apt-repository ppa:apandada1/brightness-controller -y
apt install brightness-controller -y
installedSoftware+=('brightness-controller')

#Install Chromimum
apt-get install chromium-browser -y
installedSoftware+=('chromium-browser')

#Install Skype
apt-get install skypeforlinux -y
installedSoftware+=('skype')

#Install Sublime Text
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | apt-key add -
echo "deb https://download.sublimetext.com/ apt/stable/" | tee /etc/apt/sources.list.d/sublime-text.list
apt-get update
apt-get install sublime-text -y
installedSoftware+=('sublime-text')

#Install vlc player
apt-get install vlc -y
installedSoftware+=('vlc')

#Instal TeamViewer
apt-get install teamviewer -y
installedSoftware+=('teamviewer')

#Install Rambox
snap install rambox
installedSoftware+=('rambox')

#Install Terminator
apt-get install terminator -y
installedSoftware+=('terminator')

#Install youtube-dl
sudo apt install youtube-dl -y
installedSoftware+=('youtube-dl')

#Install cerebro
apt install gdebi -y
wget https://github.com/KELiON/cerebro/releases/download/v0.3.1/cerebro_0.3.1_amd64.deb
yes | gdebi cerebro_0.3.1_amd64.deb 
installedSoftware+=('cerebro')

#Install postman
snap install postman -y
installedSoftware+=('postman')

#Install pgadmin
apt install postgresql-common -y
sh /usr/share/postgresql-common/pgdg/apt.postgresql.org.sh
apt-get install pgadmin4 -y

installedSoftware+=('pgadmin')

output="Done Successfully! \nInstalled: \n"

for i in "${installedSoftware[@]}"; do
    output="$output $i \n"
done

output="$output \nVersions: \n \n$versions"

echo -e && printf "$output" && echo -e
printf "$output" >> Logs/SetupUbuntuDevEnvRunSummary.txt
echo 'The Full Log can be Viewed in Home/Logs/SetupUbuntuDevEnvRunSummary.txt'