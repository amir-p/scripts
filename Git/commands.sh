#!/bin/bash

# Basics
git int
git status

git fetch
# will remove the remote branches info in local repository which no longer exist on the
# remote repository
git fetch --prune
git pull 
git pull --rebase 


# Stage
git add fileName.txt 
git add –u 
git add –A
git add .


# Commit 
git commit –m "Commit message" 
git commit –am "Commit message" 


# Push 
git push
git push origin master

git push origin feature1:feature1remote 
git push --force origin feature-branch 

# Push a new branch with setting automatically the upstream branch with same name
git push --set-upstream origin myBranchName
# or
git push -u origin myNewBranchName


# Remote
git remote
git remote -v
git remote rename origin myNewAlias 
# remove remote for origin
git remote rm orgin
# after you remove remote for origin you can add it as ssh(or https)
git remote add origin git@gitlab.com:user/repository-name.git


# Log, View and Show
git log
git log –oneline
git log –oneline –graph
git shortlog
git shortlog –sne

git logs origin/master 
git logs master

git show HEAD 

# untracked changes
git diff
# stagged changes
git diff --cached

git diff commit_sha_1..commit_sha_2
git diff HEAD..76789 
git diff HEAD~1..
git log –oneline –graph –all --decorate 

#view commits only made to specific branch branched from master
git log master..BranchName
git shortlog master..BranchName
git cherry -v master
git log master..


# Branches
git branch 
git branch  -r
git branch –set-upstream master origin/master 
# create a branch
git branch BranchName
git checkout -b BranchName
# create a branch from tag
git branch branchName v1.0.0 
git checkout –b branchName v1.0.0 
# create a branch from commit
git branch branchName SHA
git checkout –b branchName SHA
# delete branch in local repo
git branch –d branchName 
git branch –D branchName
# delete remote repo
git push origin –delete feature1remote
git push origin :feature1remote
# rename branch
git branch –m currentBranchName newBranchName 


# Tags
git tag
# create a tag
git tag tagName 
git tag v1.0.0 
git tag tagName SHA
# create a tag annotation
git tag –a v1.0.0 
# create a tag with signature
git tag –s v1.0.0 
# verify a tag
git tag –v v1.0.0 

git push --tags 


# Rebase
git rebase origin/master 
git rebase --continue 
git push --force-with-lease origin BranchName 
git rebase --abort
git rebase --skip 


# Squash with Rebase
git checkout BranchName
#N is number of commits to squash
git rebase --interactive HEAD~[N] 
git push --force-with-lease origin BranchName


# Git cherry pick
git checkout BranchName
git cherry-pick commit-sha
git push –force-with-lease origin BranchName


# Git Stash
git stash list  
git stash 
git stash apply stashName 

git stash push -m "message" 
git stash pop  


# Reset and revert
git checkout Filename.txt

git reset --hard 
git reset --hard HEAD

git reset --hard HEAD~1 

git reset --hard origin/master 
git reset --hard origin


# Diff and Merge tools
git mergetool
git difftool


# Aliases
git config –global alias.lga "log –oneline –graph –all --decorate" 
git lga 
