$commoncli="e:/src/git/scripts/Windows/CustomCli/"
Import-Module ($commoncli + "MyCommands.ps1")

Write-Host 'Welcome to Users Powershell session.'
Write-Host 'For more info write: cmd-help .'

function cmd-init-ubuntu() {
	docker run -it --name customcliubuntu ubuntu:latest /bin/bash
}

function cmd-remove-ubuntu() {
	docker stop customcliubuntu
	docker rm customcliubuntu	
}

function cmd-start-ubuntu() {
	docker start customcliubuntu
	docker exec -it customcliubuntu /bin/bash
}

function cmd-stop-ubuntu() {
	docker stop customcliubuntu
}
