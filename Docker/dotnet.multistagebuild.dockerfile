FROM microsoft/aspnetcore-build:2.0.0 AS build
MAINTAINER Amir Pasalic

COPY . ./code
WORKDIR /code
RUN dotnet publish TestProject.WebApi.csproj --output /output --configuration Release

FROM microsoft/aspnetcore:2.0.0
MAINTAINER Amir Pasalic
COPY --from=build /output /app
WORKDIR /app
EXPOSE 80
ENTRYPOINT ["dotnet", "TestProject.WebApi.dll"]
VOLUME /logs