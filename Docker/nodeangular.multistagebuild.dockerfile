FROM node:8.9.4 as node
WORKDIR /app
COPY . /app/
RUN npm install
RUN npm install --unsafe-perm -g @angular/cli
RUN npm run build

FROM nginx:1.13.8
COPY --from=node /app/dist/ /usr/share/nginx/html/ui/
COPY nginx-custom.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
ENTRYPOINT ["nginx", "-g", "daemon off;"]