#!/bin/bash

# Cleanup Networks
docker network ls 
docker network ls | grep "bridge"
docker network rm $(docker network ls | grep "bridge" | awk '/ / { print $1 }')

# Cleanup/Delete Vloumes
docker volume rm $(docker volume ls -qf dangling=true)
docker volume ls -qf dangling=true | xargs -r docker volume rm

# Cleanup Images
docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
docker images | grep "none"
docker rmi $(docker images | grep "none" | awk '/ / { print $3 }')

# Cleanup Containers
docker rm $(docker ps -qa --no-trunc --filter "status=exited")

# Prune commands
# General remove, Containers, Volumes, Networks and Images
# not used by at least one container
docker system prune
docker system prune -af
docker system prune -a --volumes

# View logs from Container
docker logs containerName

# View logs in real time
docker logs -f containerName
docker-compose logs -f

#Stop all running containers
docker stop $(docker ps)

# Show all exited Containers
docker ps -f "status=exited"

# Show all Containers which have part of name
docker ps -f name=containername
docker ps -f name=partOfTheName

# Display live stream resource usage statistics
docker stats containerName
docker stats -a #for all running containers


